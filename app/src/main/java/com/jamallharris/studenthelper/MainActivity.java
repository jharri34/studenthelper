package com.jamallharris.studenthelper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements QuestionRecyclerAdapter.ItemClickListener{
    public static final String QUESTION_MESSAGE = "com.jamallharris.studenthelper.QUESTION";
    public static final String ANSWER_MESSAGE = "com.jamallharris.studenthelper.ANSWER";
    public static HashMap<String,String> QA_STORE = new HashMap<>();
    QuestionRecyclerAdapter adapter;
    Boolean createdCalled = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void sendMessage(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText answerInput = (EditText) findViewById(R.id.answerInput);
        EditText questionInput = (EditText) findViewById(R.id.questionInput);

        String answer = answerInput.getText().toString();
        String question = questionInput.getText().toString();
        intent.putExtra(QUESTION_MESSAGE, question);
        intent.putExtra(ANSWER_MESSAGE, answer);
        startActivity(intent);
    }
    public void refreshButton(View view){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        Map<String, ?> questionAnswers =  preferences.getAll();

        RecyclerView questionAnswerRecyclerView = findViewById(R.id.questionAnswerRecyclerView);
        questionAnswerRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new QuestionRecyclerAdapter(this, questionAnswers);
        adapter.setClickListener(this);
        questionAnswerRecyclerView.setAdapter(adapter);

    }

    @Override
    public void onItemClick(View view, int position) {
        String question = adapter.getQuestion(position);
        String answer = adapter.getAnswer(question);
        Toast.makeText(this, "You clicked " + question + " Answers is  " + answer, Toast.LENGTH_SHORT).show();
    }
}
