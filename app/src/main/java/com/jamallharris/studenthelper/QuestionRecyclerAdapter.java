package com.jamallharris.studenthelper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class QuestionRecyclerAdapter extends RecyclerView.Adapter<QuestionRecyclerAdapter.QuesitonHolder> {
    private ItemClickListener clickListener;
    private Map<String,?> questionAnswers;
    private List<String> questions = new ArrayList();
    private LayoutInflater mInflater;

    QuestionRecyclerAdapter(Context context, Map<String,?> questionAnswers) {
        this.mInflater = LayoutInflater.from(context);
        this.questionAnswers = questionAnswers;

        for(Map.Entry<String,?> entry : questionAnswers.entrySet()){
            Log.d("",entry.getKey());
            this.questions.add(entry.getKey());
        }
    }

    @NonNull
    @Override
    public QuestionRecyclerAdapter.QuesitonHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = mInflater.inflate(R.layout.recycler_card_row, parent, false);
        return new QuesitonHolder(view);
    }

    @Override //need to figure how to bind the right data
    public void onBindViewHolder(@NonNull QuestionRecyclerAdapter.QuesitonHolder questionHolder, int i) {
        String question = questions.get(i);
        questionHolder.questionViewHolder.setText(question);
    }

    @Override
    public int getItemCount() {
        return this.questions.size();
    }

    public class QuesitonHolder  extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView questionViewHolder;

        QuesitonHolder(View itemView) {
            super(itemView);
            questionViewHolder = itemView.findViewById(R.id.questionCardText);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onItemClick(view, getAdapterPosition());
        }
    }
    String getAnswer(String quesiton) {
        return questionAnswers.get(quesiton).toString();
    }
    String getQuestion(int id) {
        return questions.get(id);
    }
    void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
