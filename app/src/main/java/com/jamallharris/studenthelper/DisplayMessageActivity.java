package com.jamallharris.studenthelper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {
    String question;
    String answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);
        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        question = intent.getStringExtra(MainActivity.QUESTION_MESSAGE);
        answer = intent.getStringExtra(MainActivity.ANSWER_MESSAGE);

        // Capture the layout's TextView and set the string as its text
        TextView questionView = findViewById(R.id.questionView);
        TextView answerView = findViewById(R.id.answerView);

        questionView.setText(question);
        answerView.setText(answer);
    }

    public void cancelButton(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void saveButton(View view){
        Intent intent = new Intent(this, MainActivity.class);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(question, answer);
        editor.apply();

        startActivity(intent);
    }
}
